/*
 * Copyright (C) 2015 essobedo.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.gitlab.essobedo.ptsv;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class provide all the info of a given player.
 *
 * @author <a href="mailto:nicolas.filotto@gmail.com">Nicolas Filotto</a>
 * @version $Id$
 */
public final class PlayerInfo {

    /**
     * The id of the player.
     */
    private int id;
    /**
     * The login of the player.
     */
    private String login;
    /**
     * The stats of the current season.
     */
    private PlayerStats currentStats;
    /**
     * The stats of the previous seasons.
     */
    private List<PlayerStats> oldStats;
    /**
     * The ranking distribution of the current trend.
     */
    private RankingDistribution currentTrend;
    /**
     * The ranking distribution of the current season.
     */
    private RankingDistribution currentDistribution;
    /**
     * The ranking distribution of the previous seasons.
     */
    private RankingDistribution globalDistribution;

    /**
     * @return The id of the player.
     */
    public int getId() {
        return this.id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    /**
     * @return The login of the player.
     */
    public String getLogin() {
        return this.login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    /**
     * @return The stats of the current season.
     */
    public PlayerStats getCurrentStats() {
        return this.currentStats;
    }

    public void setCurrentStats(final PlayerStats currentStats) {
        this.currentStats = currentStats;
    }

    /**
     * @return The stats of the previous seasons.
     */
    public List<PlayerStats> getOldStats() {
        return this.oldStats == null ? null : Collections.unmodifiableList(this.oldStats);
    }

    public void setOldStats(final List<PlayerStats> oldStats) {
        this.oldStats = oldStats;
    }

    /**
     * @return all stats of the players with at least one game played ordered from the oldest to the newest.
     */
    public List<PlayerStats> getAllStats() {
        final List<PlayerStats> stats = new ArrayList<>();
        if (this.oldStats != null) {
            this.oldStats.stream().filter(oldPlayerStats -> oldPlayerStats.getPlayed() > 0).forEach(stats::add);
        }
        stats.add(this.currentStats);
        return stats;
    }

    /**
     * @return all non filtered stats of the players with at least one game played ordered from the oldest to the newest.
     */
    public List<PlayerStats> getEveryStats() {
        final List<PlayerStats> stats = new ArrayList<>();
        if (this.oldStats != null) {
            stats.addAll(this.oldStats);
        }
        stats.add(this.currentStats);
        return stats;
    }

    /**
     * @return The ranking distribution of the previous seasons.
     */
    public RankingDistribution getGlobalDistribution() {
        return this.globalDistribution;
    }

    public void setGlobalDistribution(final RankingDistribution globalDistribution) {
        this.globalDistribution = globalDistribution;
    }

    /**
     * @return The ranking distribution of the current season.
     */
    public RankingDistribution getCurrentDistribution() {
        return this.currentDistribution;
    }

    public void setCurrentDistribution(final RankingDistribution currentDistribution) {
        this.currentDistribution = currentDistribution;
    }

    /**
     * @return The ranking distribution of the current trend.
     */
    public RankingDistribution getCurrentTrend() {
        return this.currentTrend;
    }

    public void setCurrentTrend(final RankingDistribution currentTrend) {
        this.currentTrend = currentTrend;
    }
}
