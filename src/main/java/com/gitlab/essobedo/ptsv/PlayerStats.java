/*
 * Copyright (C) 2015 essobedo.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.gitlab.essobedo.ptsv;

/**
 * Defines all the statistics of a player for a given season.
 *
 * @author <a href="mailto:nicolas.filotto@gmail.com">Nicolas Filotto</a>
 * @version $Id$
 */
public final class PlayerStats {

    /**
     * The name of the corresponding season.
     */
    private final String label;
    /**
     * The position of the player.
     */
    private int position;
    /**
     * The score of the player.
     */
    private double score;
    /**
     * The total amount of games played.
     */
    private int played;
    /**
     * The total amount of games won.
     */
    private int won;
    /**
     * The percent of games won.
     */
    private double wonPercent;
    /**
     * The placement of the player.
     */
    private int placement;
    /**
     * The average of the player.
     */
    private double average;

    public PlayerStats(final String label) {
        this.label = label;
    }

    public String getLabel() {
        return this.label;
    }

    public int getPosition() {
        return this.position;
    }

    public void setPosition(final int position) {
        this.position = position;
    }

    public double getScore() {
        return this.score;
    }

    public void setScore(final double score) {
        this.score = score;
    }

    public int getPlayed() {
        return this.played;
    }

    public void setPlayed(final int played) {
        this.played = played;
    }

    public int getWon() {
        return this.won;
    }

    public void setWon(final int won) {
        this.won = won;
    }

    public double getWonPercent() {
        return this.wonPercent;
    }

    public void setWonPercent(final double wonPercent) {
        this.wonPercent = wonPercent;
    }

    public int getPlacement() {
        return this.placement;
    }

    public void setPlacement(final int placement) {
        this.placement = placement;
    }

    public double getAverage() {
        return this.average;
    }

    public void setAverage(final double average) {
        this.average = average;
    }

    public int getTargetPosition() {
        return RankingDistribution.getTargetPosition(this.score, this.placement, this.played);
    }
}
