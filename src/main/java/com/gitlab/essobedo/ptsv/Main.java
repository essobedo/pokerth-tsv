/*
 * Copyright (C) 2015 essobedo.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.gitlab.essobedo.ptsv;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * The entry point to the application
 *
 * @author <a href="mailto:nicolas.filotto@gmail.com">Nicolas Filotto</a>
 * @version $Id$
 */
public final class Main extends Application {

    private static final ExecutorService EXECUTOR = Executors.newSingleThreadExecutor();
    private static final Map<Integer, String> COLORS = new HashMap<>();
    private static final Pattern COMMA = Pattern.compile(",");

    static {
        Main.COLORS.put(0, "f3622d");
        Main.COLORS.put(1, "fba71b");
        Main.COLORS.put(2, "57b757");
        Main.COLORS.put(3, "41a9c9");
        Main.COLORS.put(4, "4258c9");
        Main.COLORS.put(5, "9a42c8");
        Main.COLORS.put(6, "c84164");
        Main.COLORS.put(7, "888888");
    }
    /**
     * Result pane.
     */
    private Pane resultPane;

    private DoubleProperty totalStack;

    private ListProperty<PlayerInfo> data;

    private Map<String, String> lineColors = new HashMap<>();

    private Stage stage;

    public static void main(final String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(final Stage primaryStage) throws Exception {
        final VBox vBox = new VBox(10.0);
        vBox.setPadding(new Insets(10.0, 5.0, 10.0, 5.0));

        final Label label = new Label("Ranking - Game Table URL");
        label.getStyleClass().add("FieldName");

        final TextField text = new TextField(
            "https://www.pokerth.net/component/pthranking/?view=pthranking&layout=gametable&nick1=${nick1}&nick2=${nick2}" +
            "&nick3=${nick3}&nick4=${nick4}&nick5=${nick5}&nick6=${nick6}&nick7=${nick7}&nick8=${nick8}" +
            "&nick9=${nick9}&nick10=${nick10}&table=${table}"
        );
        text.getStyleClass().add("FieldValue");
        text.setMinWidth(500.0);

        text.requestFocus();
        text.selectAll();

        final Button button = new Button("Extract Data");
        button.getStyleClass().add("FieldName");

        final HBox hBox = new HBox(10.0);
        hBox.setAlignment(Pos.CENTER);
        hBox.getChildren().addAll(label, text, button);

        final Label resultLabel = new Label();
        resultLabel.setWrapText(true);

        this.resultPane = new StackPane();
        this.resultPane.setId("ResultPane");
        this.resultPane.setPadding(new Insets(5.0, 0.0, 0.0, 0.0));

        button.setOnMouseClicked(event -> this.extractData(text, resultLabel));
        text.setOnKeyReleased(
            event -> {
                if (event.getCode() == KeyCode.ENTER) {
                    this.extractData(text, resultLabel);
                }
            }
        );
        primaryStage.setTitle("PokerTH table's statistics viewer");
        vBox.getChildren().addAll(hBox, new Separator(), this.resultPane);
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        final Scene scene = new Scene(
            vBox, screenSize.getWidth() - 20.0, screenSize.getHeight() - 70.0
        );
        scene.getStylesheets().add(Main.class.getResource("/css/main.css").toString());
        primaryStage.setScene(scene);
        primaryStage.setOnCloseRequest(
            event -> Main.EXECUTOR.shutdown()
        );
        primaryStage.show();
        this.stage = primaryStage;
        Main.updateTooltipBehavior(500.0, 20_000.0, 200.0, false);
    }

    private void extractData(final TextField text, final Label resultLabel) {
        this.stage.getScene().setCursor(Cursor.WAIT);
        resultLabel.setTextFill(Color.BLACK);
        resultLabel.setText("Extracting data, please wait...");
        this.resultPane.getChildren().setAll(resultLabel);
        Main.EXECUTOR.execute(
            () -> {
                try {
                    final DataExtractor.Result res = DataExtractor.extractData(text.getText());
                    Platform.runLater(
                        () -> {
                            this.stage.getScene().setCursor(Cursor.DEFAULT);
                            if (res.success()) {
                                this.showResult(res);
                            } else {
                                resultLabel.setTextFill(Color.RED);
                                resultLabel.setText(res.message);
                            }
                        }
                    );
                } catch (final RuntimeException e1) {
                    e1.printStackTrace();
                    Platform.runLater(
                        () -> {
                            this.stage.getScene().setCursor(Cursor.DEFAULT);
                            resultLabel.setTextFill(Color.RED);
                            resultLabel.setText(e1.getLocalizedMessage());
                        }
                    );
                }
            }
        );
    }

    private void showResult(final DataExtractor.Result res) {
        this.lineColors = new HashMap<>();
        this.totalStack = new SimpleDoubleProperty(100_000.0);
        this.data = new SimpleListProperty<>(FXCollections.observableArrayList(res.infos));

        final VBox allInfo = new VBox(10.0);
        allInfo.prefWidthProperty().bind(this.stage.widthProperty().add(-30.0));
        allInfo.getStyleClass().add("AllInfo");
        allInfo.setAlignment(Pos.CENTER);

        final Label tableTitle = new Label(res.title);
        tableTitle.getStyleClass().add("Title");

        allInfo.getChildren().add(tableTitle);
        VBox.setMargin(tableTitle, new Insets(10.0));
        final HBox mainInfo = this.createMainInfo();

        final TableView<PlayerInfo> table = this.createTable();
        final HBox tableHBox = new HBox(table);
        tableHBox.setAlignment(Pos.CENTER);

        final HBox tableMainInfo = new HBox(mainInfo);
        tableMainInfo.setAlignment(Pos.CENTER);
        allInfo.getChildren().addAll(tableMainInfo, tableHBox);
        this.createLineChart(allInfo);
        this.createPieCharts(allInfo);
        this.data.addListener(
            (ListChangeListener<PlayerInfo>) c -> {
                while (c.next()) {
                    if (c.wasReplaced() && c.getRemovedSize() == 1 || !c.wasReplaced() && c.wasRemoved()) {
                        allInfo.getChildren().removeAll(allInfo.lookupAll(".Box-PieCharts"));
                        this.createPieCharts(allInfo);
                    }
                }
            }
        );
        final ScrollPane scrollPane = new ScrollPane(allInfo);
        scrollPane.prefWidthProperty().bind(this.stage.widthProperty());
        scrollPane.prefHeightProperty().bind(this.stage.heightProperty());
        this.resultPane.getChildren().setAll(scrollPane);
    }

    private void createLineChart(final VBox allInfo) {
        allInfo.getChildren().add(
            new HBox(
                5.0,
                this.createLineChart("Position", PlayerStats::getPosition, 3),
                this.createLineChart("Score", PlayerStats::getScore, 3),
                this.createLineChart("Average", PlayerStats::getAverage, 3)
            )
        );
        allInfo.getChildren().add(
            new HBox(
                5.0,
                this.createLineChart("Placement", PlayerStats::getPlacement, 4),
                this.createLineChart("Won Percent", PlayerStats::getWonPercent, 4),
                this.createLineChart("Won", PlayerStats::getWon, 4),
                this.createLineChart("Played", PlayerStats::getPlayed, 4)
            )
        );
    }

    private void createPieCharts(final VBox allInfo) {
        HBox hBox = new HBox(5.0);
        hBox.getStyleClass().add("Box-PieCharts");
        int counter = 0;
        for (final PlayerInfo info : this.data) {
            if (counter++ % 4 == 0 && counter > 0) {
                allInfo.getChildren().add(hBox);
                hBox = new HBox(5.0);
                hBox.getStyleClass().add("Box-PieCharts");
            }
            hBox.getChildren().add(this.createPieChart(info));
        }
        allInfo.getChildren().add(hBox);
    }

    private HBox createMainInfo() {
        final HBox mainInfo = new HBox(20.0);
        mainInfo.setPadding(new Insets(10.0));

        final Label avgStackTitle = new Label("Average Stack:");
        avgStackTitle.getStyleClass().add("FieldName");

        final Label avgStackValue = new Label();
        avgStackValue.getStyleClass().add("FieldValue");
        avgStackValue.textProperty().bind(
            Bindings.format(Locale.US, "$%,.2f", this.totalStack.divide(this.data.sizeProperty()))
        );

        final Label ttStackTitle = new Label("Total Stack:");
        ttStackTitle.getStyleClass().add("FieldName");

        final StackPane editableValue = new StackPane();
        final Label ttStackValue = new Label();
        ttStackValue.getStyleClass().add("FieldValue");
        ttStackValue.textProperty().bind(Bindings.format(Locale.US, "$%,.0f", this.totalStack));

        final TextField textField = new TextField();
        textField.setOnKeyReleased(
            event -> {
                if (event.getCode() == KeyCode.ENTER) {
                    avgStackValue.requestFocus();
                }
            }
        );
        textField.setVisible(false);
        textField.setDisable(false);
        textField.focusedProperty().addListener(
            (observable, oldValue, newValue) -> {
                if (!newValue) {
                    this.onInputOver(ttStackValue, textField);
                }
            }
        );

        ttStackValue.setOnMouseClicked(
            event -> {
                ttStackValue.setVisible(false);
                textField.setLayoutX(ttStackValue.getLayoutX());
                textField.setLayoutY(ttStackValue.getLayoutY());
                textField.setVisible(true);
                textField.setText(String.format(Locale.US, "%,d", (int) this.totalStack.get()));
                textField.requestFocus();

            }
        );
        editableValue.getChildren().addAll(ttStackValue, textField);
        StackPane.setAlignment(ttStackValue, Pos.TOP_LEFT);
        StackPane.setAlignment(textField, Pos.TOP_LEFT);

        final ImageView imageViewRefresh = new ImageView(new Image(Main.class.getResource("/img/refresh.png").toString()));
        imageViewRefresh.setFitWidth(16.0);
        imageViewRefresh.setPreserveRatio(true);

        // The refresh button
        final Button btnRefresh = new Button();
        btnRefresh.setId("BtnRefresh");
        btnRefresh.setGraphic(imageViewRefresh);
        btnRefresh.setTooltip(new Tooltip("Refresh data"));
        btnRefresh.setOnAction(event -> this.refresh());

        mainInfo.getChildren().addAll(btnRefresh, avgStackTitle, avgStackValue, ttStackTitle, editableValue);
        return mainInfo;
    }

    private TableView<PlayerInfo> createTable() {
        final TableView<PlayerInfo> table = new TableView<>(this.data);
        table.prefWidthProperty().bind(this.stage.widthProperty().add(-10));
        table.setFixedCellSize(28.0);
        table.prefHeightProperty().bind(Bindings.size(table.getItems()).multiply(table.getFixedCellSize()).add(40));
        table.setId("ListPlayer");

        final TableColumn<PlayerInfo, Integer> indexColumn = new TableColumn<>("#");
        indexColumn.prefWidthProperty().bind(table.widthProperty().divide(26).add(-2));
        indexColumn.setResizable(false);
        indexColumn.setEditable(false);
        indexColumn.setSortable(false);
        indexColumn.setCellFactory(param -> new Main.IndexTableCell<>());
        final TableColumn<PlayerInfo, Integer> positionColumn = new TableColumn<>();
        final Label positionLabel = new Label("Position");
        positionLabel.setTooltip(new Tooltip("The rank in the current season."));
        positionColumn.setGraphic(positionLabel);

        positionColumn.prefWidthProperty().bind(table.widthProperty().divide(13).add(-2));
        positionColumn.setSortable(true);
        positionColumn.setEditable(false);
        positionColumn.setCellValueFactory(
            param -> new ReadOnlyObjectWrapper<>(param.getValue().getCurrentStats().getPosition())
        );
        final TableColumn<PlayerInfo, Hyperlink> loginColumn = new TableColumn<>();
        final Label loginLabel = new Label("Login");
        loginLabel.setTooltip(new Tooltip("The login of the players."));
        loginColumn.setGraphic(loginLabel);

        loginColumn.prefWidthProperty().bind(table.widthProperty().divide(13).multiply(2).add(-2));
        loginColumn.setComparator(Comparator.comparing(Labeled::getText));
        loginColumn.setResizable(false);
        loginColumn.setSortable(true);
        loginColumn.setEditable(false);
        loginColumn.setCellValueFactory(
            param -> {
                final Hyperlink link = new Hyperlink(param.getValue().getLogin());
                link.setOnAction(
                    evt -> {
                        final Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
                        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
                            try {
                                desktop.browse(
                                    new URI(
                                        String.format(
                                            "https://www.pokerth.net/component/pthranking/?view=pthranking&layout=profile&userid=%d",
                                            param.getValue().getId()
                                        )
                                    )
                                );
                            } catch (final IOException | URISyntaxException e) {
                                throw new IllegalStateException(e);
                            }
                        }
                    }
                );
                return new ReadOnlyObjectWrapper<>(link);
            }
        );
        final TableColumn<PlayerInfo, Double> scoreColumn = new TableColumn<>();
        final Label scoreLabel = new Label("Score");
        scoreLabel.setTooltip(new Tooltip("The score in the current season."));
        scoreColumn.setGraphic(scoreLabel);

        scoreColumn.prefWidthProperty().bind(table.widthProperty().divide(13).add(-2));
        scoreColumn.setComparator(scoreColumn.getComparator().reversed());
        scoreColumn.setResizable(false);
        scoreColumn.setSortable(true);
        scoreColumn.setEditable(false);
        scoreColumn.setCellValueFactory(
            param -> new ReadOnlyObjectWrapper<>(param.getValue().getCurrentStats().getScore())
        );
        final TableColumn<PlayerInfo, Double> currentTrendColumn = new TableColumn<>();
        final Label currentTrendLabel = new Label("Current\n Trend");
        currentTrendLabel.setTooltip(new Tooltip("The average score of the 5 last games."));
        currentTrendColumn.setGraphic(currentTrendLabel);

        currentTrendColumn.prefWidthProperty().bind(table.widthProperty().divide(13).add(-2));
        currentTrendColumn.setComparator(currentTrendColumn.getComparator().reversed());
        currentTrendColumn.setResizable(false);
        currentTrendColumn.setSortable(true);
        currentTrendColumn.setEditable(false);
        currentTrendColumn.setCellValueFactory(
            param -> new ReadOnlyObjectWrapper<>(param.getValue().getCurrentTrend().getAvg())
        );
        final TableColumn<PlayerInfo, Double> seasonAvgColumn = new TableColumn<>();
        final Label seasonAvgLabel = new Label("Season\n  Avg");
        seasonAvgLabel.setTooltip(new Tooltip("The average score of the current season."));
        seasonAvgColumn.setGraphic(seasonAvgLabel);

        seasonAvgColumn.prefWidthProperty().bind(table.widthProperty().divide(13).add(-2));
        seasonAvgColumn.setComparator(seasonAvgColumn.getComparator().reversed());
        seasonAvgColumn.setResizable(false);
        seasonAvgColumn.setSortable(true);
        seasonAvgColumn.setEditable(false);
        seasonAvgColumn.setCellValueFactory(
            param -> new ReadOnlyObjectWrapper<>(param.getValue().getCurrentStats().getAverage())
        );
        final TableColumn<PlayerInfo, Double> globalAvgColumn = new TableColumn<>();
        final Label globalAvgLabel = new Label("Global\n  Avg");
        globalAvgLabel.setTooltip(new Tooltip("The average score of all seasons."));
        globalAvgColumn.setGraphic(globalAvgLabel);

        globalAvgColumn.prefWidthProperty().bind(table.widthProperty().divide(13).add(-2));
        globalAvgColumn.setComparator(globalAvgColumn.getComparator().reversed());
        globalAvgColumn.setResizable(false);
        globalAvgColumn.setSortable(true);
        globalAvgColumn.setEditable(false);
        globalAvgColumn.setCellValueFactory(
            param -> new ReadOnlyObjectWrapper<>(param.getValue().getGlobalDistribution().getAvg())
        );
        final TableColumn<PlayerInfo, Integer> seasonGamesColumn = new TableColumn<>();
        final Label seasonGamesLabel = new Label("Season\nGames");
        seasonGamesLabel.setTooltip(new Tooltip("The total amount of games played in the current season."));
        seasonGamesColumn.setGraphic(seasonGamesLabel);

        seasonGamesColumn.prefWidthProperty().bind(table.widthProperty().divide(13).add(-2));
        seasonGamesColumn.setComparator(seasonGamesColumn.getComparator().reversed());
        seasonGamesColumn.setResizable(false);
        seasonGamesColumn.setSortable(true);
        seasonGamesColumn.setEditable(false);
        seasonGamesColumn.setCellValueFactory(
            param -> new ReadOnlyObjectWrapper<>(param.getValue().getCurrentStats().getPlayed())
        );
        final TableColumn<PlayerInfo, Integer> globalGamesColumn = new TableColumn<>();
        final Label globalGamesLabel = new Label(" Total\nGames");
        globalGamesLabel.setTooltip(new Tooltip("The total amount of games played in all seasons."));
        globalGamesColumn.setGraphic(globalGamesLabel);

        globalGamesColumn.prefWidthProperty().bind(table.widthProperty().divide(13).add(-2));
        globalGamesColumn.setComparator(globalGamesColumn.getComparator().reversed());
        globalGamesColumn.setResizable(false);
        globalGamesColumn.setSortable(true);
        globalGamesColumn.setEditable(false);
        globalGamesColumn.setCellValueFactory(
            param -> new ReadOnlyObjectWrapper<>(param.getValue().getGlobalDistribution().getTotal())
        );
        final TableColumn<PlayerInfo, Integer> totalPointsColumn = new TableColumn<>();
        final Label totalPointsLabel = new Label(" Total\nPoints");
        totalPointsLabel.setTooltip(new Tooltip("The total amount of points in the current season."));
        totalPointsColumn.setGraphic(totalPointsLabel);

        totalPointsColumn.prefWidthProperty().bind(table.widthProperty().divide(13).add(-2));
        totalPointsColumn.setComparator(totalPointsColumn.getComparator().reversed());
        totalPointsColumn.setResizable(false);
        totalPointsColumn.setSortable(true);
        totalPointsColumn.setEditable(false);
        totalPointsColumn.setCellValueFactory(
            param -> new ReadOnlyObjectWrapper<>(param.getValue().getCurrentStats().getPlacement())
        );
        final TableColumn<PlayerInfo, Integer> targetPositionColumn = new TableColumn<>();
        final Label targetPositionLabel = new Label(" Target\nPosition");
        targetPositionLabel.setTooltip(new Tooltip("The position to at least reach to improve the current score."));
        targetPositionColumn.setGraphic(targetPositionLabel);

        targetPositionColumn.prefWidthProperty().bind(table.widthProperty().divide(13).add(-2));
        targetPositionColumn.setResizable(false);
        targetPositionColumn.setSortable(true);
        targetPositionColumn.setEditable(false);
        targetPositionColumn.setCellValueFactory(
            param -> new ReadOnlyObjectWrapper<>(param.getValue().getCurrentStats().getTargetPosition())
        );
        final TableColumn<PlayerInfo, Integer> idColumn = new TableColumn<>();
        final Label idLabel = new Label("Id");
        idLabel.setTooltip(new Tooltip("The id of the players."));
        idColumn.setGraphic(idLabel);

        idColumn.prefWidthProperty().bind(table.widthProperty().divide(13).add(-2));
        idColumn.setResizable(false);
        idColumn.setSortable(true);
        idColumn.setEditable(false);
        idColumn.setCellValueFactory(
            param -> new ReadOnlyObjectWrapper<>(param.getValue().getId())
        );
        final TableColumn<PlayerInfo, Boolean> deleteColumn = new TableColumn<>("Del.");
        deleteColumn.prefWidthProperty().bind(table.widthProperty().divide(26).add(-2));
        deleteColumn.setResizable(false);
        deleteColumn.setEditable(false);
        deleteColumn.setSortable(false);
        deleteColumn.setCellFactory(param -> new Main.RemoveTableCell());
        deleteColumn.setCellValueFactory(param -> new SimpleBooleanProperty(param.getValue() != null));

        //noinspection unchecked
        table.getColumns().addAll(
            indexColumn, positionColumn, loginColumn, scoreColumn, currentTrendColumn, seasonAvgColumn,
            globalAvgColumn, seasonGamesColumn, globalGamesColumn, totalPointsColumn, targetPositionColumn,
            idColumn, deleteColumn
        );
        table.getSortOrder().add(positionColumn);
        return table;
    }

    private void refresh() {
        Main.EXECUTOR.execute(
            () -> {
                try {
                    DataExtractor.refreshData(this.data);
                } catch (final RuntimeException e1) {
                    e1.printStackTrace();
                }
            }
        );
    }

    private void onInputOver(final Label ttStackValue, final TextField textField) {
        try {
            final boolean delta = !textField.getText().isEmpty() && textField.getText().charAt(0) == '+'
                || !textField.getText().isEmpty() && textField.getText().charAt(0) == '-';
            final int value = Integer.parseInt(Main.COMMA.matcher(textField.getText()).replaceAll(""));
            if (delta) {
                this.totalStack.setValue(this.totalStack.get() + (double) value);
            } else {
                this.totalStack.setValue(value);
            }
        } catch (final NumberFormatException e) {
            e.printStackTrace();
        }
        ttStackValue.setVisible(true);
        textField.setVisible(false);
    }

    private Node createPieChart(final PlayerInfo info) {
        final TitledPane gridTitlePane = new TitledPane();
        gridTitlePane.setText(info.getLogin());

        final VBox vBox = new VBox();
        final Collection<PieChart.Data> dataCurrentDistribution = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            dataCurrentDistribution.add(
                new PieChart.Data(Integer.toString(i), (double) info.getCurrentDistribution().getTotal(i))
            );
        }
        final PieChart chartCurrentDistribution = new PieChart(
            FXCollections.observableArrayList(dataCurrentDistribution)
        );
        chartCurrentDistribution.setTitle("Current Distribution");
        vBox.getChildren().add(chartCurrentDistribution);
        for (final PieChart.Data data : chartCurrentDistribution.getData()) {
            final Tooltip tooltip = new Tooltip();
            final VBox vb = new VBox(2.0);
            final Label title = new Label(data.getName());
            title.getStyleClass().add("Tooltip-Title");
            vb.getChildren().add(title);

            final Label value = new Label(
                String.format(
                    "%d (%.1f %%)",
                    (int) data.getPieValue(),
                    100.0 * data.getPieValue() / (double) info.getCurrentDistribution().getTotal()
                )
            );
            value.getStyleClass().add("Tooltip-Value");
            vb.getChildren().add(value);
            tooltip.setGraphic(vb);
            Tooltip.install(data.getNode(), tooltip);
        }

        final Collection<PieChart.Data> dataGlobalDistribution = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            dataGlobalDistribution.add(
                new PieChart.Data(Integer.toString(i), (double) info.getGlobalDistribution().getTotal(i))
            );
        }
        final PieChart chartGlobalDistribution = new PieChart(
            FXCollections.observableArrayList(dataGlobalDistribution)
        );
        chartGlobalDistribution.setTitle("Global Distribution");
        vBox.getChildren().add(chartGlobalDistribution);
        for (final PieChart.Data data : chartGlobalDistribution.getData()) {
            final Tooltip tooltip = new Tooltip();
            final VBox vb = new VBox(2.0);
            final Label title = new Label(data.getName());
            title.getStyleClass().add("Tooltip-Title");
            vb.getChildren().add(title);

            final Label value = new Label(
                String.format(
                    "%d (%.1f %%)",
                    (int) data.getPieValue(),
                    100.0 * data.getPieValue() / (double) info.getGlobalDistribution().getTotal()
                )
            );
            value.getStyleClass().add("Tooltip-Value");
            vb.getChildren().add(value);
            tooltip.setGraphic(vb);
            Tooltip.install(data.getNode(), tooltip);
        }
        this.data.addListener(
            (ListChangeListener<PlayerInfo>) change -> {
                while (change.next()) {
                    if (!change.wasReplaced() && change.wasRemoved()) {
                        change.getRemoved().stream()
                            .map(PlayerInfo::getLogin)
                            .filter(login -> login.equals(info.getLogin()))
                            .forEach(
                                login -> {
                                    for (final PieChart.Data data : chartCurrentDistribution.getData()) {
                                        Tooltip.uninstall(data.getNode(), null);
                                    }
                                    for (final PieChart.Data data : chartGlobalDistribution.getData()) {
                                        Tooltip.uninstall(data.getNode(), null);
                                    }
                                }
                            );
                    }
                }
            }
        );
        gridTitlePane.setContent(vBox);
        gridTitlePane.prefWidthProperty().bind(this.stage.widthProperty().add(-60).divide(4));
        return gridTitlePane;
    }

    private Node createLineChart(final String name, final Function<PlayerStats, Number> extractor, final int totalByRaw) {
        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Seasons");
        final LineChart<String, Number> lineChart = new LineChart<>(xAxis, yAxis);
        lineChart.setTitle(name);

        for (final PlayerInfo info : this.data) {
            final XYChart.Series<String, Number> series = new XYChart.Series<>();
            series.setName(info.getLogin());
            for (final PlayerStats stats : info.getEveryStats()) {
                series.getData().add(
                    new XYChart.Data<>(stats.getLabel(), extractor.apply(stats), info.getLogin())
                );
            }
            lineChart.getData().add(series);
        }

        this.data.addListener(
            (ListChangeListener<PlayerInfo>) change -> {
                while (change.next()) {
                    if (change.wasReplaced() && change.getRemovedSize() == 1) {
                        final PlayerInfo info = change.getList().get(change.getFrom());
                        lineChart.getData()
                            .stream()
                            .filter(series -> series.getName().equals(info.getLogin()))
                            .forEach(
                                series -> {
                                    boolean updated = false;
                                    final List<PlayerStats> allStats = info.getEveryStats();
                                    int i = 0;
                                    for (final XYChart.Data<String, Number> data : series.getData()) {
                                        final Number currentValue = data.getYValue();
                                        final PlayerStats stats = allStats.get(i);
                                        if (data.getXValue().equals(stats.getLabel())) {
                                            final Number newValue = extractor.apply(stats);
                                            if (!currentValue.equals(newValue)) {
                                                updated = true;
                                                data.setYValue(newValue);
                                            }
                                            i++;
                                        }
                                    }
                                    if (updated) {
                                        this.generateTooltip(lineChart);
                                    }
                                }
                            );
                    } else if (!change.wasReplaced() && change.wasRemoved()) {
                        change.getRemoved().stream()
                            .map(PlayerInfo::getLogin)
                            .forEach(
                                login -> {
                                    lineChart.getData().forEach(
                                        data -> Tooltip.uninstall(data.getNode(), null)
                                    );
                                    lineChart.getData().removeAll(
                                        lineChart.getData().stream()
                                            .filter(series -> series.getName().equals(login))
                                            .collect(Collectors.toList())
                                    );
                                    this.generateTooltip(lineChart);
                                }
                            );
                    }
                }
            }
        );

        this.generateTooltip(lineChart);

        final TitledPane gridTitlePane = new TitledPane();
        gridTitlePane.setText(name);
        gridTitlePane.setContent(lineChart);
        gridTitlePane.prefWidthProperty().bind(this.stage.widthProperty().divide(totalByRaw));
        return gridTitlePane;
    }

    private void generateTooltip(final LineChart<String, Number> lineChart) {
        int count = 0;
        final Map<String, String> styles = new HashMap<>();
        final boolean init = this.lineColors.isEmpty();
        for (final XYChart.Series<String, Number> series : lineChart.getData()) {
            final String color;
            if (init) {
                color = Main.COLORS.get(count++ % 8);
                this.lineColors.put(series.getName(), color);
            } else {
                color = this.lineColors.get(series.getName());
            }
            styles.put(series.getName(), String.format("-fx-stroke: #%s", color));
        }
        lineChart.getData().stream()
            .map(XYChart.Series::getData)
            .flatMap(Collection::stream)
            .forEach(
                data -> {
                    final Tooltip tooltip = new Tooltip();
                    final VBox vb = new VBox(2.0);
                    final Label title = new Label(data.getXValue());
                    title.getStyleClass().add("Tooltip-Title");
                    vb.getChildren().add(title);
                    lineChart.getData().stream()
                        .map(XYChart.Series::getData)
                        .flatMap(Collection::stream)
                        .filter(currentData -> data.getXValue().equals(currentData.getXValue()))
                        .sorted((d1, d2) -> -Double.compare(d1.getYValue().doubleValue(), d2.getYValue().doubleValue()))
                        .forEach(
                            currentData -> {
                                final Circle circle = new Circle();
                                circle.getStyleClass().add("Tooltip-Circle");
                                //noinspection SuspiciousMethodCalls
                                circle.setStyle(styles.get(currentData.getExtraValue()));
                                circle.setRadius(4.0);

                                final Label login = new Label(currentData.getExtraValue() + ":");
                                login.getStyleClass().add("Tooltip-Login");

                                final Label value = new Label(currentData.getYValue().toString());
                                value.getStyleClass().add("Tooltip-Value");

                                vb.getChildren().add(new HBox(2.0, circle, login, value));
                                HBox.setMargin(circle, new Insets(4.0, 0, 0, 0));
                            }
                        );
                    tooltip.setGraphic(vb);
                    Tooltip.install(data.getNode(), tooltip);
                }
            );
    }

    /**
     * Hack allowing to modify the default behavior of the tooltips.
     * @param openDelay The open delay, knowing that by default it is set to 1000.
     * @param visibleDuration The visible duration, knowing that by default it is set to 5000.
     * @param closeDelay The close delay, knowing that by default it is set to 200.
     * @param hideOnExit Indicates whether the tooltip should be hide on exit,
     * knowing that by default it is set to false.
     */
    private static void updateTooltipBehavior(final double openDelay, final double visibleDuration,
        final double closeDelay, final boolean hideOnExit) {
        try {
            // Get the non public field "BEHAVIOR"
            final Field fieldBehavior = Tooltip.class.getDeclaredField("BEHAVIOR");
            // Make the field accessible to be able to get and set its value
            fieldBehavior.setAccessible(true);
            // Get the value of the static field
            final Object objBehavior = fieldBehavior.get(null);
            // Get the constructor of the private static inner class TooltipBehavior
            final Constructor<?> constructor = objBehavior.getClass().getDeclaredConstructor(
                Duration.class, Duration.class, Duration.class, boolean.class
            );
            // Make the constructor accessible to be able to invoke it
            constructor.setAccessible(true);
            // Create a new instance of the private static inner class TooltipBehavior
            final Object tooltipBehavior = constructor.newInstance(
                new Duration(openDelay),
                new Duration(visibleDuration), //visible duration default 5000
                new Duration(closeDelay),  //close delay default 200
                hideOnExit // hide en exit
            );
            // Set the new instance of TooltipBehavior
            fieldBehavior.set(null, tooltipBehavior);
        } catch (final Exception e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * The inner class allowing to the show the index of the table row in the table.
     *
     * @param <S> The type of the TableView generic type (i.e. S == TableView&lt;S&gt;).
     *           This should also match with the first generic type in TableColumn.
     * @param <T> The type of the item contained within the Cell.
     */
    private static final class IndexTableCell<S, T> extends TableCell<S, T> {

        @Override
        public void updateItem(final T item, final boolean empty) {
            super.updateItem(item, empty);
            this.setGraphic(null);
            if (empty || this.getTableRow() == null) {
                this.setText(null);
            } else {
                this.setText(Integer.toString(this.getTableRow().getIndex() + 1));
            }
        }
    }

    /**
     * The inner class allowing to display the remove action in the table.
     */
    private static final class RemoveTableCell extends TableCell<PlayerInfo, Boolean> {

        private static final Image REMOVE = new Image(Main.class.getResource("/img/remove.png").toString());

        /**
         * The remove button.
         */
        private final Button btnRemove;

        /**
         * The default constructor.
         */
        RemoveTableCell() {
            final ImageView imageViewRemove = new ImageView(RemoveTableCell.REMOVE);
            imageViewRemove.setFitWidth(16.0);
            imageViewRemove.setPreserveRatio(true);

            // The remove button
            this.btnRemove = new Button();
            this.btnRemove.setId("BtnRemove");
            this.btnRemove.setGraphic(imageViewRemove);
            this.btnRemove.setTooltip(new Tooltip("Delete player"));
            this.btnRemove.setOnAction(event -> this.getTableView().getItems().remove(this.getIndex()));
        }

        @Override
        public void updateItem(final Boolean item, final boolean empty) {
            super.updateItem(item, empty);
            if (empty) {
                this.setGraphic(null);
            } else {
                this.setGraphic(this.btnRemove);
            }
        }
    }
}
