/*
 * Copyright (C) 2015 essobedo.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.gitlab.essobedo.ptsv;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javafx.application.Platform;
import javafx.beans.property.ListProperty;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import rx.Observable;
import rx.observables.SyncOnSubscribe;
import rx.schedulers.Schedulers;

/**
 * This class is responsible for the data extraction from the profile pages.
 *
 * @author <a href="mailto:nicolas.filotto@gmail.com">Nicolas Filotto</a>
 * @version $Id$
 */
public final class DataExtractor {

    /**
     * The pattern used to extract the logins from the url of pokerth.
     */
    private static final Pattern LOGIN_PATTERN = Pattern.compile("nick\\d+=([^&]+)");
    /**
     * The pattern used to extract the name of the table from the url of pokerth.
     */
    private static final Pattern TABLE_PATTERN = Pattern.compile("&table=(.*)");
    /**
     * The pattern used to extract the rank of the player.
     */
    private static final Pattern RANK_PATTERN = Pattern.compile("(\\d+) / (\\d+)");
    /**
     * The pattern used to extract the main info from the profile page.
     */
    private static final String MAIN_INFO_PATTERN = "tr td:containsOwn(%s:) + td";
    /**
     * The pattern used to extract the games of the ranking distribution from the profile page.
     */
    private static final String GAMES_RANKING_DISTRIB_PATTERN = "h3:containsOwn(%s) + div table tr th:containsOwn(Games:) ~ td";
    /**
     * The pattern used to extract the percent of the ranking distribution from the profile page.
     */
    private static final String PERCENT_RANKING_DISTRIB_PATTERN = "h3:containsOwn(%s) + div table tr td:containsOwn(Percent:) ~ td";
    /**
     * The pattern used to extract the player stats of the previous seasons from the profile page.
     */
    private static final String OLD_STATS_PATTERN = "h2:containsOwn(Historical Seasons:) + *:matches(Statistics for .*:) ~ div table";
    /**
     * The format used for the last positions.
     */
    private static final Pattern POSITIONS_FORMAT = Pattern.compile(", ");

    /**
     * Default constructor.
     */
    private DataExtractor() {
    }

    /**
     * Extracts the data using the given parameters.
     *
     * @param parameters the parameters used for the extraction
     * @return the result of the extraction
     */
    static DataExtractor.Result extractData(final CharSequence parameters) {
        return DataExtractor.extractLogin(parameters).flatMap(
            login ->
                Observable.fromCallable(
                    () ->
                        DataExtractor.extractInfo(
                            String.format(
                                "https://www.pokerth.net/component/pthranking/?view=pthranking&layout=profile&username=%s",
                                login
                            )
                        )
                ).subscribeOn(Schedulers.io())
        ).toList().flatMap(
            playerInfos ->
                Observable.fromCallable(
                    () -> {
                        if (playerInfos.isEmpty()) {
                            return new DataExtractor.Result("Could not extract any login");
                        }
                        return new DataExtractor.Result(DataExtractor.extractTitle(parameters), DataExtractor.completeStats(playerInfos));
                    }
                )
        ).onErrorReturn(th -> new DataExtractor.Result(String.format("Could not extract the data: %s", th.getMessage()))).toBlocking().single();
    }

    static void refreshData(final ListProperty<PlayerInfo> playerInfos) {
        Observable.from(playerInfos).map(PlayerInfo::getLogin).flatMap(
            login ->
                Observable.fromCallable(
                    () ->
                        DataExtractor.extractInfo(
                            String.format(
                                "https://www.pokerth.net/component/pthranking/?view=pthranking&layout=profile&username=%s",
                                login
                            )
                        )
                ).subscribeOn(Schedulers.io())
        ).toList().flatMap(
            infos -> Observable.from(DataExtractor.completeStats(infos))
        ).forEach(
            info -> {
                for (int i = 0; i < playerInfos.size(); i++) {
                    if (info.getLogin().equals(playerInfos.get(i).getLogin())) {
                        final int index = i;
                        Platform.runLater(() -> playerInfos.set(index, info));
                        break;
                    }
                }
            }
        );
    }

    /**
     * Completes the statistics of the players by adding missing player stats'season.
     *
     * @param playerInfos the information of the players to complete
     * @return the provided collection of player info that has been completed
     */
    static Collection<PlayerInfo> completeStats(final Collection<PlayerInfo> playerInfos) {
        final Collection<String> labels = new TreeSet<>(
            (s1, s2) -> {
                if (s1.equals(s2)) {
                    return 0;
                } else if (s1.charAt(0) == 'C') {
                    return 1;
                } else if (s2.charAt(0) == 'C') {
                    return -1;
                } else if (s1.charAt(0) == 'B' && s2.charAt(0) == '2') {
                    return -1;
                } else if (s1.charAt(0) == '2' && s2.charAt(0) == 'B') {
                    return 1;
                }
                return s1.compareTo(s2);
            }
        );
        labels.addAll(
            playerInfos.stream()
                .flatMap(pi -> pi.getAllStats().stream())
                .map(PlayerStats::getLabel)
                .collect(Collectors.toList())
        );
        //int i = 1;
        for (final PlayerInfo playerInfo : playerInfos) {
            //playerInfo.setLogin("nick" + i++);
            final List<PlayerStats> allStats = playerInfo.getAllStats();
            final List<PlayerStats> newStats = new ArrayList<>(labels.size());
            int index = 0;
            for (final String label : labels) {
                if (index < allStats.size() && allStats.get(index).getLabel().equals(label)) {
                    newStats.add(allStats.get(index++));
                } else {
                    newStats.add(new PlayerStats(label));
                }
            }
            playerInfo.setCurrentStats(newStats.remove(newStats.size() - 1));
            playerInfo.setOldStats(newStats);
        }
        return playerInfos;
    }

    /**
     * Extracts the logins from the provided parameters.
     *
     * @param parameters the url or parameters of the normal comparison page of pokerth
     * @return the extracted logins
     */
    static Observable<String> extractLogin(final CharSequence parameters) {
        return Observable.create(
            SyncOnSubscribe.createSingleState(
                () -> DataExtractor.LOGIN_PATTERN.matcher(parameters),
                (matcher, observer) -> {
                    if (matcher.find()) {
                        try {
                            observer.onNext(URLDecoder.decode(matcher.group(1), "UTF-8"));
                        } catch (final UnsupportedEncodingException e) {
                            observer.onError(e);
                        }
                    } else {
                        observer.onCompleted();
                    }
                }
            )
        );
    }

    /**
     * Extracts the information of a player from the content available at the provided URL.
     *
     * @param url the url of the profile of the player
     * @return the information of a player
     */
    static PlayerInfo extractInfo(final String url) {
        final PlayerInfo info;
        try {
            final Document doc = Jsoup.connect(url).get();
            info = new PlayerInfo();
            Element element = doc.select(String.format(DataExtractor.MAIN_INFO_PATTERN, "Player id")).first();
            if (element == null) {
                throw new RuntimeException(String.format("Could not extract the id of the player from %s", url));
            }
            info.setId(Integer.parseInt(element.text()));
            element = doc.select(String.format(DataExtractor.MAIN_INFO_PATTERN, "Name")).first();
            if (element == null) {
                throw new RuntimeException(String.format("Could not extract the login of the player from %s", url));
            }
            info.setLogin(element.text());
            element = doc.select(String.format(DataExtractor.MAIN_INFO_PATTERN, "Rank")).first();
            if (element == null) {
                throw new RuntimeException(String.format("Could not extract the current position of the player from %s", url));
            }
            final PlayerStats stats = new PlayerStats("Current");
            stats.setPosition(DataExtractor.extractPosition(element.text()));
            element = doc.select(String.format(DataExtractor.MAIN_INFO_PATTERN, "Final Score")).first();
            if (element == null) {
                throw new RuntimeException(String.format("Could not extract the current score of the player from %s", url));
            }
            stats.setScore(Double.parseDouble(element.text().substring(0, element.text().indexOf(" %"))));
            element = doc.select(String.format(DataExtractor.MAIN_INFO_PATTERN, "Games")).first();
            if (element == null) {
                throw new RuntimeException(String.format("Could not extract the total amount of games played of the player from %s", url));
            }
            stats.setPlayed(Integer.parseInt(element.text()));
            element = doc.select(String.format(DataExtractor.MAIN_INFO_PATTERN, "Total points")).first();
            if (element == null) {
                throw new RuntimeException(String.format("Could not extract the placement of the player from %s", url));
            }
            stats.setPlacement(Integer.parseInt(element.text()));
            element = doc.select(String.format(DataExtractor.MAIN_INFO_PATTERN, "Average Points")).first();
            if (element == null) {
                throw new RuntimeException(String.format("Could not extract the average of the player from %s", url));
            }
            stats.setAverage(Double.parseDouble(element.text()));
            info.setCurrentStats(stats);
            Elements games = doc.select(String.format(DataExtractor.GAMES_RANKING_DISTRIB_PATTERN, "Statistics for current season"));
            Elements percent = doc.select(String.format(DataExtractor.PERCENT_RANKING_DISTRIB_PATTERN, "Statistics for current season"));
            if (games.size() != 11 || percent.size() != 11) {
                throw new RuntimeException(String.format("Could not extract the current values of rank of the player from %s", url));
            }
            RankingDistribution distribution = new RankingDistribution();
            for (int i = 1; i <= 10; i++) {
                distribution.setTotal(i, Integer.parseInt(games.get(i - 1).text()));
                distribution.setPercent(i, Double.parseDouble(percent.get(i - 1).text().substring(0, percent.get(i - 1).text().indexOf(" %"))));
            }
            stats.setWon(distribution.getTotal(1));
            stats.setWonPercent(distribution.getPercent(1));
            info.setCurrentDistribution(distribution);
            games = doc.select(String.format(DataExtractor.GAMES_RANKING_DISTRIB_PATTERN, "Statistics for all time"));
            percent = doc.select(String.format(DataExtractor.PERCENT_RANKING_DISTRIB_PATTERN, "Statistics for all time"));
            distribution = new RankingDistribution();
            if (!games.isEmpty() || !percent.isEmpty()) {
                if (games.size() != 11 || percent.size() != 11) {
                    throw new RuntimeException(String.format("Could not extract the global values of rank of the player from %s", url));
                }
                for (int i = 1; i <= 10; i++) {
                    distribution.setTotal(i, Integer.parseInt(games.get(i - 1).text()));
                    distribution.setPercent(
                        i,
                        Double.parseDouble(
                            percent.get(i - 1).text().substring(0, percent.get(i - 1).text().indexOf(" %"))
                        )
                    );
                }
            }
            info.setGlobalDistribution(distribution);
            distribution = new RankingDistribution();
            element = doc.select(String.format(DataExtractor.MAIN_INFO_PATTERN, "Last 5 game places")).first();
            if (element != null) {
                final String[] positions = DataExtractor.POSITIONS_FORMAT.split(element.text());
                for (final String position : positions) {
                    final int positionVal = Integer.parseInt(position);
                    distribution.setTotal(positionVal, distribution.getTotal(positionVal) + 1);
                    distribution.setPercent(positionVal, (double) distribution.getTotal(positionVal) / (double) positions.length);
                }
            }
            info.setCurrentTrend(distribution);
            final Elements oldStatsElements = doc.select(DataExtractor.OLD_STATS_PATTERN);
            List<PlayerStats> oldStats = null;
            int position = 0;
            for (final Element current : oldStatsElements) {
                games = current.select("tr th:containsOwn(Games:) ~ td");
                if (games.isEmpty()) {
                    // Final score table so we ignore it
                    final Elements globalResults = current.select("tr:contains(Final Score) ~ tr td:eq(4)");
                    if (globalResults.isEmpty()) {
                        throw new RuntimeException(String.format("Could not extract the global results of the player from %s", url));
                    }
                    position = Integer.parseInt(globalResults.first().text());
                    continue;
                }
                if (oldStats == null) {
                    oldStats = new ArrayList<>();
                    info.setOldStats(oldStats);
                }
                final String label = current.parent().previousElementSibling().text();
                final PlayerStats currentStats = new PlayerStats(label.substring("Statistics for ".length(), label.length() - 1));

                percent = current.select("tr td:containsOwn(Percent:) ~ td");
                distribution = new RankingDistribution();
                for (int i = 1; i <= 10; i++) {
                    distribution.setTotal(i, Integer.parseInt(games.get(i - 1).text()));
                    distribution.setPercent(
                        i,
                        Double.parseDouble(
                            percent.get(i - 1).text().substring(0, percent.get(i - 1).text().indexOf(" %"))
                        )
                    );
                }
                currentStats.setPosition(position);
                position = 0;
                currentStats.setScore(distribution.getScore());
                currentStats.setPlayed(distribution.getTotal());
                currentStats.setPlacement(distribution.getTotalPoints());
                currentStats.setAverage(distribution.getAvg());
                currentStats.setWon(distribution.getTotal(1));
                currentStats.setWonPercent(distribution.getPercent(1));
                oldStats.add(0, currentStats);
            }
        } catch (final RuntimeException | IOException e) {
            System.err.println(
                String.format(
                    "Could not load the info of the player from %s due to the following error: %s",
                    url, e.getMessage()
                )
            );
            throw new RuntimeException(
                String.format(
                    "Could not load the info of the player from %s due to the following error: %s",
                    url, e.getMessage()
                ), e
            );
        }

        return info;
    }

    /**
     * Extracts the title of the generated content from the provided parameters.
     *
     * @param parameters the url or parameters of the normal comparison page of pokerth
     * @return "Table: " followed by the name of the table if it can be extracted otherwise it will be
     * "Table's stats"
     */
    private static String extractTitle(final CharSequence parameters) throws UnsupportedEncodingException {
        final Matcher matcher = DataExtractor.TABLE_PATTERN.matcher(parameters);
        if (matcher.find()) {
            return String.format("Table: %s", URLDecoder.decode(matcher.group(1), "UTF-8"));
        }
        return "Table's stats";
    }

    /**
     * Extracts the position of the user from the given text knowing that the text should be in the format \d+ / \d+
     * @param text the text from which we extract the position.
     * @return the position of the user that has been extracted, {@code -1} otherwise.
     */
    private static int extractPosition(String text) {
        final Matcher matcher = DataExtractor.RANK_PATTERN.matcher(text);
        if (matcher.find()) {
            return Integer.parseInt(matcher.group(1));
        }
        return -1;
    }

    static final class Result {
        String message;
        String title;
        Collection<PlayerInfo> infos;

        private Result(final String message) {
            this.message = message;
        }

        private Result(final String title, final Collection<PlayerInfo> infos) {
            this.title = title;
            this.infos = infos;
        }

        boolean success() {
            return this.infos != null;
        }
    }
}
