/*
 * Copyright (C) 2015 essobedo.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.gitlab.essobedo.ptsv;

/**
 * This class described a ranking distribution from 1 to 10.
 *
 * @author <a href="mailto:nicolas.filotto@gmail.com">Nicolas Filotto</a>
 * @version $Id$
 */
public final class RankingDistribution {

    private static final int[] POINTS = {15, 9, 6, 4, 3, 2, 1, 0, 0, 0};

    /**
     * The percent for each rank.
     */
    private final double[] percent = new double[10];
    /**
     * The total amount of games for each rank.
     */
    private final int[] total = new int[10];

    /**
     * @return The percent for a given rank
     */
    public double getPercent(final int rank) {
        return this.percent[rank - 1];
    }

    /**
     * Sets the percent of a given rank.
     */
    public void setPercent(final int rank, final double percent) {
        this.percent[rank - 1] = percent;
    }

    /**
     * @return The total amount of games of a given rank
     */
    public int getTotal(final int rank) {
        return this.total[rank - 1];
    }

    /**
     * Sets the total amount of games of a given rank
     */
    public void setTotal(final int rank, final int total) {
        this.total[rank - 1] = total;
    }

    /**
     * @return the total amount of games
     */
    public int getTotal() {
        int result = 0;
        for (final int val : this.total) {
            result += val;
        }
        return result;
    }

    /**
     * @return the total amount of point
     */
    public int getTotalPoints() {
        int result = 0;
        for (int i = 0; i < this.total.length; i++) {
            result += this.total[i] * RankingDistribution.POINTS[i];
        }
        return result;
    }

    /**
     * @return the global average
     */
    public double getAvg() {
        final double result = (double) this.getTotalPoints() / (double) this.getTotal();
        return (double) Math.round(result * 100.0) / 100.0;
    }

    /**
     * @return the score
     */
    public double getScore() {
        return RankingDistribution.getScore(this.getTotalPoints(), this.getTotal());
    }

    /**
     * @param totalPoints the total of points
     * @param totalGames the total of games
     * @return the corresponding score
     */
    public static double getScore(final int totalPoints, final int totalGames) {
        final double result = 25.0 * (double) totalPoints / (10.0 + (double) totalGames);
        return (double) Math.round(result * 100.0) / 100.0;
    }

    /**
     * @param score the current score
     * @param totalPoints the current total of points
     * @param totalGames the current total of games
     * @return the position to get to improve the score
     */
    public static int getTargetPosition(final double score, final int totalPoints, final int totalGames) {
        for (int i = RankingDistribution.POINTS.length - 1; i >= 0; i--) {
            if (score < RankingDistribution.getScore(totalPoints + RankingDistribution.POINTS[i], totalGames + 1)) {
                return i + 1;
            }
        }
        return 1;
    }
}
