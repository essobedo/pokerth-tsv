/*
 * Copyright (C) 2015 essobedo.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.gitlab.essobedo.ptsv;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.After;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

/**
 * @author <a href="mailto:nicolas.filotto@gmail.com">Nicolas Filotto</a>
 * @version $Id$
 */
public final class TestDataExtractor {
    // Base URI the Grizzly HTTP server will listen on
    public static final String BASE_URI = "http://localhost:8880/TestDataExtractor";

    private HttpServer server;

    @Before
    public void init() throws Exception {
        // create a resource config that scans for JAX-RS resources and providers
        // in com.example package
        final ResourceConfig rc = new ResourceConfig().packages("com.gitlab.essobedo.ptsv");
        this.server = GrizzlyHttpServerFactory.createHttpServer(URI.create(TestDataExtractor.BASE_URI), rc);
        this.server.start();
    }

    @After
    public void end() {
        this.server.shutdown();
    }

    @Test
    public void extractLogin() throws Exception {
        assertTrue(DataExtractor.extractLogin("").toList().toBlocking().single().isEmpty());
        assertTrue(DataExtractor.extractLogin("view=pthranking").toList().toBlocking().single().isEmpty());
        assertTrue(DataExtractor.extractLogin("view=pthranking&").toList().toBlocking().single().isEmpty());
        assertTrue(DataExtractor.extractLogin("view=pthranking&layout=gametable&table=Brian").toList().toBlocking().single().isEmpty());
        List<String> result = DataExtractor.extractLogin("view=pthranking&layout=gametable&nick1=essobedo&table=Brian").toList().toBlocking().single();
        assertEquals(1L, (long) result.size());
        assertEquals("essobedo", result.get(0));
        result = DataExtractor.extractLogin("view=pthranking&layout=gametable&nick1=essobedo&nick2=Generalmos&nick3=DSAZocker&nick4=PokerBilly&nick5=" +
                "reysalomon&nick6=michelb&nick7=Knackers&nick8=delphina&nick9=remlh&table=Brian").toList().toBlocking().single();
        assertEquals(9L, (long) result.size());
        assertEquals("essobedo", result.get(0));
        assertEquals("Generalmos", result.get(1));
        assertEquals("DSAZocker", result.get(2));
        assertEquals("PokerBilly", result.get(3));
        assertEquals("reysalomon", result.get(4));
        assertEquals("michelb", result.get(5));
        assertEquals("Knackers", result.get(6));
        assertEquals("delphina", result.get(7));
        assertEquals("remlh", result.get(8));
        result = DataExtractor.extractLogin("view=pthranking&layout=gametable&nick1=essobedo&nick2=Generalmos&nick3=DSAZocker&nick4=PokerBilly&nick5=" +
                "reysalomon&nick6=michelb&nick7=Knackers&nick8=delphina&nick9=remlh&nick10=remlh%202&table=Brian").toList().toBlocking().single();
        assertEquals(10L, (long) result.size());
        assertEquals("essobedo", result.get(0));
        assertEquals("Generalmos", result.get(1));
        assertEquals("DSAZocker", result.get(2));
        assertEquals("PokerBilly", result.get(3));
        assertEquals("reysalomon", result.get(4));
        assertEquals("michelb", result.get(5));
        assertEquals("Knackers", result.get(6));
        assertEquals("delphina", result.get(7));
        assertEquals("remlh", result.get(8));
        assertEquals("remlh 2", result.get(9));
        result = DataExtractor.extractLogin("https://www.pokerth.net/component/pthranking/?view=pthranking&layout=gametable" +
            "&nick1=essobedo&nick2=NOTORIUS2&nick3=mamic&nick4=Emme&nick5=BIG5&nick6=Paolo&nick7=shyguy" +
            "&nick8=samara&nick9=L%C3%B6we&nick10=huugycom&table=5-5").toList().toBlocking().single();
        assertEquals(10L, (long) result.size());
        assertEquals("essobedo", result.get(0));
        assertEquals("NOTORIUS2", result.get(1));
        assertEquals("mamic", result.get(2));
        assertEquals("Emme", result.get(3));
        assertEquals("BIG5", result.get(4));
        assertEquals("Paolo", result.get(5));
        assertEquals("shyguy", result.get(6));
        assertEquals("samara", result.get(7));
        assertEquals("Löwe", result.get(8));
        assertEquals("huugycom", result.get(9));
    }

    @Test
    public void extractInfo() {
        try {
            DataExtractor.extractInfo(String.format("%s/foo", TestDataExtractor.BASE_URI));
            Assert.fail("A RuntimeException is expected");
        } catch (final RuntimeException e) {
            //expected
        }
        // Test 1
        PlayerInfo info = DataExtractor.extractInfo(String.format("%s/test1", TestDataExtractor.BASE_URI));
        assertEquals("darmax99", info.getLogin());
        assertEquals(47L, (long) info.getId());
        assertEquals(65L, (long) info.getCurrentStats().getPosition());
        assertEquals(139.58, info.getCurrentStats().getScore(), 0.0);
        assertEquals(290L, (long) info.getCurrentStats().getPlayed());
        assertEquals(47L, (long) info.getCurrentStats().getWon());
        assertEquals(16.2, info.getCurrentStats().getWonPercent(), 0.0);
        assertEquals(1675L, (long) info.getCurrentStats().getPlacement());
        assertEquals(5.78, info.getCurrentStats().getAverage(), 0.0);

        assertEquals(1L, (long) info.getCurrentTrend().getTotal(1));
        assertEquals(0.2, info.getCurrentTrend().getPercent(1), 0.0);
        assertEquals(0L, (long) info.getCurrentTrend().getTotal(2));
        assertEquals(0.0, info.getCurrentTrend().getPercent(2), 0.0);
        assertEquals(0L, (long) info.getCurrentTrend().getTotal(3));
        assertEquals(0.0, info.getCurrentTrend().getPercent(3), 0.0);
        assertEquals(0L, (long) info.getCurrentTrend().getTotal(4));
        assertEquals(0.0, info.getCurrentTrend().getPercent(4), 0.0);
        assertEquals(1L, (long) info.getCurrentTrend().getTotal(5));
        assertEquals(0.2, info.getCurrentTrend().getPercent(5), 0.0);
        assertEquals(1L, (long) info.getCurrentTrend().getTotal(6));
        assertEquals(0.2, info.getCurrentTrend().getPercent(6), 0.0);
        assertEquals(1L, (long) info.getCurrentTrend().getTotal(7));
        assertEquals(0.2, info.getCurrentTrend().getPercent(7), 0.0);
        assertEquals(0L, (long) info.getCurrentTrend().getTotal(8));
        assertEquals(0.0, info.getCurrentTrend().getPercent(8), 0.0);
        assertEquals(0L, (long) info.getCurrentTrend().getTotal(9));
        assertEquals(0.0, info.getCurrentTrend().getPercent(9), 0.0);
        assertEquals(1L, (long) info.getCurrentTrend().getTotal(10));
        assertEquals(0.2, info.getCurrentTrend().getPercent(10), 0.0);

        assertEquals(47L, (long) info.getCurrentDistribution().getTotal(1));
        assertEquals(16.2, info.getCurrentDistribution().getPercent(1), 0.0);
        assertEquals(46L, (long) info.getCurrentDistribution().getTotal(2));
        assertEquals(15.9, info.getCurrentDistribution().getPercent(2), 0.0);
        assertEquals(43L, (long) info.getCurrentDistribution().getTotal(3));
        assertEquals(14.8, info.getCurrentDistribution().getPercent(3), 0.0);
        assertEquals(32L, (long) info.getCurrentDistribution().getTotal(4));
        assertEquals(11.0, info.getCurrentDistribution().getPercent(4), 0.0);
        assertEquals(31L, (long) info.getCurrentDistribution().getTotal(5));
        assertEquals(10.7, info.getCurrentDistribution().getPercent(5), 0.0);
        assertEquals(31L, (long) info.getCurrentDistribution().getTotal(6));
        assertEquals(10.7, info.getCurrentDistribution().getPercent(6), 0.0);
        assertEquals(15L, (long) info.getCurrentDistribution().getTotal(7));
        assertEquals(5.2, info.getCurrentDistribution().getPercent(7), 0.0);
        assertEquals(18L, (long) info.getCurrentDistribution().getTotal(8));
        assertEquals(6.2, info.getCurrentDistribution().getPercent(8), 0.0);
        assertEquals(14L, (long) info.getCurrentDistribution().getTotal(9));
        assertEquals(4.8, info.getCurrentDistribution().getPercent(9), 0.0);
        assertEquals(13L, (long) info.getCurrentDistribution().getTotal(10));
        assertEquals(4.5, info.getCurrentDistribution().getPercent(10), 0.0);

        assertEquals(204L, (long) info.getGlobalDistribution().getTotal(1));
        assertEquals(16.1, info.getGlobalDistribution().getPercent(1), 0.0);
        assertEquals(173L, (long) info.getGlobalDistribution().getTotal(2));
        assertEquals(13.7, info.getGlobalDistribution().getPercent(2), 0.0);
        assertEquals(181L, (long) info.getGlobalDistribution().getTotal(3));
        assertEquals(14.3, info.getGlobalDistribution().getPercent(3), 0.0);
        assertEquals(176L, (long) info.getGlobalDistribution().getTotal(4));
        assertEquals(13.9, info.getGlobalDistribution().getPercent(4), 0.0);
        assertEquals(145L, (long) info.getGlobalDistribution().getTotal(5));
        assertEquals(11.5, info.getGlobalDistribution().getPercent(5), 0.0);
        assertEquals(107L, (long) info.getGlobalDistribution().getTotal(6));
        assertEquals(8.5, info.getGlobalDistribution().getPercent(6), 0.0);
        assertEquals(87L, (long) info.getGlobalDistribution().getTotal(7));
        assertEquals(6.9, info.getGlobalDistribution().getPercent(7), 0.0);
        assertEquals(77L, (long) info.getGlobalDistribution().getTotal(8));
        assertEquals(6.1, info.getGlobalDistribution().getPercent(8), 0.0);
        assertEquals(55L, (long) info.getGlobalDistribution().getTotal(9));
        assertEquals(4.4, info.getGlobalDistribution().getPercent(9), 0.0);
        assertEquals(59L, (long) info.getGlobalDistribution().getTotal(10));
        assertEquals(4.7, info.getGlobalDistribution().getPercent(10), 0.0);

        List<PlayerStats> oldStats = info.getOldStats();
        assertEquals(2L, (long) oldStats.size());

        PlayerStats stats = oldStats.get(0);
        assertEquals("2016-4", stats.getLabel());
        assertEquals(90L, (long) stats.getPosition());
        assertEquals(140.42, stats.getScore(), 0.0);
        assertEquals(632L, (long) stats.getPlayed());
        assertEquals(104L, (long) stats.getWon());
        assertEquals(16.5, stats.getWonPercent(), 0.0);
        assertEquals(3606L, (long) stats.getPlacement());
        assertEquals(5.71, stats.getAverage(), 0.0);

        stats = oldStats.get(1);
        assertEquals("2017-1", stats.getLabel());
        assertEquals(144L, (long) stats.getPosition());
        assertEquals(132.24, stats.getScore(), 0.0);
        assertEquals(342L, (long) stats.getPlayed());
        assertEquals(53L, (long) stats.getWon());
        assertEquals(15.5, stats.getWonPercent(), 0.0);
        assertEquals(1862L, (long) stats.getPlacement());
        assertEquals(5.44, stats.getAverage(), 0.0);

        // Test 2
        info = DataExtractor.extractInfo(String.format("%s/test2", TestDataExtractor.BASE_URI));
        assertEquals("essobedo", info.getLogin());
        assertEquals(5816L, (long) info.getId());
        assertEquals(2542L, (long) info.getCurrentStats().getPosition());
        assertEquals(12.5, info.getCurrentStats().getScore(), 0.0);
        assertEquals(2L, (long) info.getCurrentStats().getPlayed());
        assertEquals(0L, (long) info.getCurrentStats().getWon());
        assertEquals(0.0, info.getCurrentStats().getWonPercent(), 0.0);
        assertEquals(6L, (long) info.getCurrentStats().getPlacement());
        assertEquals(3.0, info.getCurrentStats().getAverage(), 0.0);

        assertEquals(0L, (long) info.getCurrentDistribution().getTotal(1));
        assertEquals(0.0, info.getCurrentDistribution().getPercent(1), 0.0);
        assertEquals(0L, (long) info.getCurrentDistribution().getTotal(2));
        assertEquals(0.0, info.getCurrentDistribution().getPercent(2), 0.0);
        assertEquals(1L, (long) info.getCurrentDistribution().getTotal(3));
        assertEquals(50.0, info.getCurrentDistribution().getPercent(3), 0.0);
        assertEquals(0L, (long) info.getCurrentDistribution().getTotal(4));
        assertEquals(0.0, info.getCurrentDistribution().getPercent(4), 0.0);
        assertEquals(0L, (long) info.getCurrentDistribution().getTotal(5));
        assertEquals(0.0, info.getCurrentDistribution().getPercent(5), 0.0);
        assertEquals(0L, (long) info.getCurrentDistribution().getTotal(6));
        assertEquals(0.0, info.getCurrentDistribution().getPercent(6), 0.0);
        assertEquals(0L, (long) info.getCurrentDistribution().getTotal(7));
        assertEquals(0.0, info.getCurrentDistribution().getPercent(7), 0.0);
        assertEquals(1L, (long) info.getCurrentDistribution().getTotal(8));
        assertEquals(50.0, info.getCurrentDistribution().getPercent(8), 0.0);
        assertEquals(0L, (long) info.getCurrentDistribution().getTotal(9));
        assertEquals(0.0, info.getCurrentDistribution().getPercent(9), 0.0);
        assertEquals(0L, (long) info.getCurrentDistribution().getTotal(10));
        assertEquals(0.0, info.getCurrentDistribution().getPercent(10), 0.0);

        assertEquals(7L, (long) info.getGlobalDistribution().getTotal(1));
        assertEquals(17.1, info.getGlobalDistribution().getPercent(1), 0.0);
        assertEquals(12L, (long) info.getGlobalDistribution().getTotal(2));
        assertEquals(29.3, info.getGlobalDistribution().getPercent(2), 0.0);
        assertEquals(5L, (long) info.getGlobalDistribution().getTotal(3));
        assertEquals(12.2, info.getGlobalDistribution().getPercent(3), 0.0);
        assertEquals(3L, (long) info.getGlobalDistribution().getTotal(4));
        assertEquals(7.3, info.getGlobalDistribution().getPercent(4), 0.0);
        assertEquals(4L, (long) info.getGlobalDistribution().getTotal(5));
        assertEquals(9.8, info.getGlobalDistribution().getPercent(5), 0.0);
        assertEquals(4L, (long) info.getGlobalDistribution().getTotal(6));
        assertEquals(9.8, info.getGlobalDistribution().getPercent(6), 0.0);
        assertEquals(2L, (long) info.getGlobalDistribution().getTotal(7));
        assertEquals(4.9, info.getGlobalDistribution().getPercent(7), 0.0);
        assertEquals(2L, (long) info.getGlobalDistribution().getTotal(8));
        assertEquals(4.9, info.getGlobalDistribution().getPercent(8), 0.0);
        assertEquals(1L, (long) info.getGlobalDistribution().getTotal(9));
        assertEquals(2.4, info.getGlobalDistribution().getPercent(9), 0.0);
        assertEquals(1L, (long) info.getGlobalDistribution().getTotal(10));
        assertEquals(2.4, info.getGlobalDistribution().getPercent(10), 0.0);

        oldStats = info.getOldStats();
        assertEquals(1L, (long) oldStats.size());

        stats = oldStats.get(0);
        assertEquals("2017-1", stats.getLabel());
        assertEquals(76L, (long) stats.getPosition());
        assertEquals(138.27, stats.getScore(), 0.0);
        assertEquals(39L, (long) stats.getPlayed());
        assertEquals(7L, (long) stats.getWon());
        assertEquals(17.9, stats.getWonPercent(), 0.0);
        assertEquals(271L, (long) stats.getPlacement());
        assertEquals(6.95, stats.getAverage(), 0.0);

        // Test 3
        info = DataExtractor.extractInfo(String.format("%s/test3", TestDataExtractor.BASE_URI));
        assertEquals("bennysway", info.getLogin());
        assertEquals(6109L, (long) info.getId());
        assertEquals(6956L, (long) info.getCurrentStats().getPosition());
        assertEquals(0.0, info.getCurrentStats().getScore(), 0.0);
        assertEquals(0L, (long) info.getCurrentStats().getPlayed());
        assertEquals(0L, (long) info.getCurrentStats().getWon());
        assertEquals(0.0, info.getCurrentStats().getWonPercent(), 0.0);
        assertEquals(0L, (long) info.getCurrentStats().getPlacement());
        assertEquals(0.0, info.getCurrentStats().getAverage(), 0.0);

        assertEquals(0L, (long) info.getCurrentDistribution().getTotal(1));
        assertEquals(0.0, info.getCurrentDistribution().getPercent(1), 0.0);
        assertEquals(0L, (long) info.getCurrentDistribution().getTotal(2));
        assertEquals(0.0, info.getCurrentDistribution().getPercent(2), 0.0);
        assertEquals(0L, (long) info.getCurrentDistribution().getTotal(3));
        assertEquals(0.0, info.getCurrentDistribution().getPercent(3), 0.0);
        assertEquals(0L, (long) info.getCurrentDistribution().getTotal(4));
        assertEquals(0.0, info.getCurrentDistribution().getPercent(4), 0.0);
        assertEquals(0L, (long) info.getCurrentDistribution().getTotal(5));
        assertEquals(0.0, info.getCurrentDistribution().getPercent(5), 0.0);
        assertEquals(0L, (long) info.getCurrentDistribution().getTotal(6));
        assertEquals(0.0, info.getCurrentDistribution().getPercent(6), 0.0);
        assertEquals(0L, (long) info.getCurrentDistribution().getTotal(7));
        assertEquals(0.0, info.getCurrentDistribution().getPercent(7), 0.0);
        assertEquals(0L, (long) info.getCurrentDistribution().getTotal(8));
        assertEquals(0.0, info.getCurrentDistribution().getPercent(8), 0.0);
        assertEquals(0L, (long) info.getCurrentDistribution().getTotal(9));
        assertEquals(0.0, info.getCurrentDistribution().getPercent(9), 0.0);
        assertEquals(0L, (long) info.getCurrentDistribution().getTotal(10));
        assertEquals(0.0, info.getCurrentDistribution().getPercent(10), 0.0);

        assertEquals(0L, (long) info.getGlobalDistribution().getTotal(1));
        assertEquals(0.0, info.getGlobalDistribution().getPercent(1), 0.0);
        assertEquals(0L, (long) info.getGlobalDistribution().getTotal(2));
        assertEquals(0.0, info.getGlobalDistribution().getPercent(2), 0.0);
        assertEquals(0L, (long) info.getGlobalDistribution().getTotal(3));
        assertEquals(0.0, info.getGlobalDistribution().getPercent(3), 0.0);
        assertEquals(0L, (long) info.getGlobalDistribution().getTotal(4));
        assertEquals(0.0, info.getGlobalDistribution().getPercent(4), 0.0);
        assertEquals(0L, (long) info.getGlobalDistribution().getTotal(5));
        assertEquals(0.0, info.getGlobalDistribution().getPercent(5), 0.0);
        assertEquals(0L, (long) info.getGlobalDistribution().getTotal(6));
        assertEquals(0.0, info.getGlobalDistribution().getPercent(6), 0.0);
        assertEquals(0L, (long) info.getGlobalDistribution().getTotal(7));
        assertEquals(0.0, info.getGlobalDistribution().getPercent(7), 0.0);
        assertEquals(0L, (long) info.getGlobalDistribution().getTotal(8));
        assertEquals(0.0, info.getGlobalDistribution().getPercent(8), 0.0);
        assertEquals(1L, (long) info.getGlobalDistribution().getTotal(9));
        assertEquals(100.0, info.getGlobalDistribution().getPercent(9), 0.0);
        assertEquals(0L, (long) info.getGlobalDistribution().getTotal(10));
        assertEquals(0.0, info.getGlobalDistribution().getPercent(10), 0.0);

        oldStats = info.getOldStats();
        assertEquals(1L, (long) oldStats.size());

        stats = oldStats.get(0);
        assertEquals("2017-1", stats.getLabel());
        assertEquals(3714L, (long) stats.getPosition());
        assertEquals(0.0, stats.getScore(), 0.0);
        assertEquals(1L, (long) stats.getPlayed());
        assertEquals(0L, (long) stats.getWon());
        assertEquals(0.0, stats.getWonPercent(), 0.0);
        assertEquals(0L, (long) stats.getPlacement());
        assertEquals(0.0, stats.getAverage(), 0.0);

        // Test 4
        info = DataExtractor.extractInfo(String.format("%s/test4", TestDataExtractor.BASE_URI));
        assertEquals("Löwe", info.getLogin());
        assertEquals(1651L, (long) info.getId());
        assertEquals(1733L, (long) info.getCurrentStats().getPosition());
        assertEquals(39.73, info.getCurrentStats().getScore(), 0.0);
        assertEquals(248L, (long) info.getCurrentStats().getPlayed());
        assertEquals(5L, (long) info.getCurrentStats().getWon());
        assertEquals(2.0, info.getCurrentStats().getWonPercent(), 0.0);
        assertEquals(410L, (long) info.getCurrentStats().getPlacement());
        assertEquals(1.65, info.getCurrentStats().getAverage(), 0.0);

        // Test 5
        info = DataExtractor.extractInfo(String.format("%s/test5", TestDataExtractor.BASE_URI));
        assertEquals("schoumat", info.getLogin());
        assertEquals(9751L, (long) info.getId());
        assertEquals(8451L, (long) info.getCurrentStats().getPosition());
        assertEquals(0.0, info.getCurrentStats().getScore(), 0.0);
        assertEquals(0L, (long) info.getCurrentStats().getPlayed());
        assertEquals(0L, (long) info.getCurrentStats().getWon());
        assertEquals(0.0, info.getCurrentStats().getWonPercent(), 0.0);
        assertEquals(0L, (long) info.getCurrentStats().getPlacement());
        assertEquals(0.0, info.getCurrentStats().getAverage(), 0.0);

        assertEquals(0L, (long) info.getCurrentDistribution().getTotal(1));
        assertEquals(0.0, info.getCurrentDistribution().getPercent(1), 0.0);
        assertEquals(0L, (long) info.getCurrentDistribution().getTotal(2));
        assertEquals(0.0, info.getCurrentDistribution().getPercent(2), 0.0);
        assertEquals(0L, (long) info.getCurrentDistribution().getTotal(3));
        assertEquals(0.0, info.getCurrentDistribution().getPercent(3), 0.0);
        assertEquals(0L, (long) info.getCurrentDistribution().getTotal(4));
        assertEquals(0.0, info.getCurrentDistribution().getPercent(4), 0.0);
        assertEquals(0L, (long) info.getCurrentDistribution().getTotal(5));
        assertEquals(0.0, info.getCurrentDistribution().getPercent(5), 0.0);
        assertEquals(0L, (long) info.getCurrentDistribution().getTotal(6));
        assertEquals(0.0, info.getCurrentDistribution().getPercent(6), 0.0);
        assertEquals(0L, (long) info.getCurrentDistribution().getTotal(7));
        assertEquals(0.0, info.getCurrentDistribution().getPercent(7), 0.0);
        assertEquals(0L, (long) info.getCurrentDistribution().getTotal(8));
        assertEquals(0.0, info.getCurrentDistribution().getPercent(8), 0.0);
        assertEquals(0L, (long) info.getCurrentDistribution().getTotal(9));
        assertEquals(0.0, info.getCurrentDistribution().getPercent(9), 0.0);
        assertEquals(0L, (long) info.getCurrentDistribution().getTotal(10));
        assertEquals(0.0, info.getCurrentDistribution().getPercent(10), 0.0);

        assertEquals(0L, (long) info.getGlobalDistribution().getTotal(1));
        assertEquals(0.0, info.getGlobalDistribution().getPercent(1), 0.0);
        assertEquals(0L, (long) info.getGlobalDistribution().getTotal(2));
        assertEquals(0.0, info.getGlobalDistribution().getPercent(2), 0.0);
        assertEquals(0L, (long) info.getGlobalDistribution().getTotal(3));
        assertEquals(0.0, info.getGlobalDistribution().getPercent(3), 0.0);
        assertEquals(0L, (long) info.getGlobalDistribution().getTotal(4));
        assertEquals(0.0, info.getGlobalDistribution().getPercent(4), 0.0);
        assertEquals(0L, (long) info.getGlobalDistribution().getTotal(5));
        assertEquals(0.0, info.getGlobalDistribution().getPercent(5), 0.0);
        assertEquals(0L, (long) info.getGlobalDistribution().getTotal(6));
        assertEquals(0.0, info.getGlobalDistribution().getPercent(6), 0.0);
        assertEquals(0L, (long) info.getGlobalDistribution().getTotal(7));
        assertEquals(0.0, info.getGlobalDistribution().getPercent(7), 0.0);
        assertEquals(0L, (long) info.getGlobalDistribution().getTotal(8));
        assertEquals(0.0, info.getGlobalDistribution().getPercent(8), 0.0);
        assertEquals(0L, (long) info.getGlobalDistribution().getTotal(9));
        assertEquals(0.0, info.getGlobalDistribution().getPercent(9), 0.0);
        assertEquals(0L, (long) info.getGlobalDistribution().getTotal(10));
        assertEquals(0.0, info.getGlobalDistribution().getPercent(10), 0.0);

        assertNull(info.getOldStats());
    }

    @Test
    public void extractData() throws Exception {
        final DataExtractor.Result result = DataExtractor.extractData(
            "http://pokerth.net/redirect_user_profile.php?tableview=1&nick9=Löwe&table=5-5_"
        );
        assertTrue(result.success());
    }

    @Test
    public void completeStats() {
        final Collection<PlayerInfo> info = new ArrayList<>();
        DataExtractor.completeStats(info);
        assertEquals(0L, (long) info.size());

        final PlayerInfo infoP1 = DataExtractor.extractInfo(String.format("%s/test1", TestDataExtractor.BASE_URI));
        infoP1.setLogin("t/est\"1");
        info.add(infoP1);
        DataExtractor.completeStats(info);

        assertEquals(1L, (long) info.size());

        PlayerInfo objInfoPlayer = info.iterator().next();

        // login
        assertEquals("t/est\"1", objInfoPlayer.getLogin());
        // id
        assertEquals(47L, (long) objInfoPlayer.getId());
        // placement
        assertEquals(1675L, (long) objInfoPlayer.getCurrentStats().getPlacement());
        // targetPosition
        assertEquals(3L, (long) objInfoPlayer.getCurrentStats().getTargetPosition());
        // currentTrend
        assertEquals(4.2, objInfoPlayer.getCurrentTrend().getAvg(), 0.0);

        // currentTotal1
        assertEquals(47L, (long) objInfoPlayer.getCurrentDistribution().getTotal(1));
        // currentPercent1
        assertEquals(16.2, objInfoPlayer.getCurrentDistribution().getPercent(1), 0.0);
        // currentTotal2
        assertEquals(46L, (long) objInfoPlayer.getCurrentDistribution().getTotal(2));
        // currentPercent2
        assertEquals(15.9, objInfoPlayer.getCurrentDistribution().getPercent(2), 0.0);
        // currentTotal3
        assertEquals(43L, (long) objInfoPlayer.getCurrentDistribution().getTotal(3));
        // currentPercent3
        assertEquals(14.8, objInfoPlayer.getCurrentDistribution().getPercent(3), 0.0);
        // currentTotal4
        assertEquals(32L, (long) objInfoPlayer.getCurrentDistribution().getTotal(4));
        // currentPercent4
        assertEquals(11.0, objInfoPlayer.getCurrentDistribution().getPercent(4), 0.0);
        // currentTotal5
        assertEquals(31L, (long) objInfoPlayer.getCurrentDistribution().getTotal(5));
        // currentPercent5
        assertEquals(10.7, objInfoPlayer.getCurrentDistribution().getPercent(5), 0.0);
        // currentTotal6
        assertEquals(31L, (long) objInfoPlayer.getCurrentDistribution().getTotal(6));
        assertEquals(10.7, objInfoPlayer.getCurrentDistribution().getPercent(6), 0.0);
        // currentTotal7
        assertEquals(15L, (long) objInfoPlayer.getCurrentDistribution().getTotal(7));
        // currentPercent7
        assertEquals(5.2, objInfoPlayer.getCurrentDistribution().getPercent(7), 0.0);
        // currentTotal8
        assertEquals(18L, (long) objInfoPlayer.getCurrentDistribution().getTotal(8));
        // currentPercent8
        assertEquals(6.2, objInfoPlayer.getCurrentDistribution().getPercent(8), 0.0);
        // currentTotal9
        assertEquals(14L, (long) objInfoPlayer.getCurrentDistribution().getTotal(9));
        // currentPercent9
        assertEquals(4.8, objInfoPlayer.getCurrentDistribution().getPercent(9), 0.0);
        // currentTotal10
        assertEquals(13L, (long) objInfoPlayer.getCurrentDistribution().getTotal(10));
        // currentPercent10
        assertEquals(4.5, objInfoPlayer.getCurrentDistribution().getPercent(10), 0.0);

        // currentTotal
        assertEquals(290L, (long) objInfoPlayer.getCurrentDistribution().getTotal());
        // currentAvg
        assertEquals(5.78, objInfoPlayer.getCurrentDistribution().getAvg(), 0.0);

        // globalTotal1
        assertEquals(204L, (long) objInfoPlayer.getGlobalDistribution().getTotal(1));
        assertEquals(16.1, objInfoPlayer.getGlobalDistribution().getPercent(1), 0.0);
        // globalTotal2
        assertEquals(173L, (long) objInfoPlayer.getGlobalDistribution().getTotal(2));
        assertEquals(13.7, objInfoPlayer.getGlobalDistribution().getPercent(2), 0.0);
        // globalTotal3
        assertEquals(181L, (long) objInfoPlayer.getGlobalDistribution().getTotal(3));
        assertEquals(14.3, objInfoPlayer.getGlobalDistribution().getPercent(3), 0.0);
        // globalTotal4
        assertEquals(176L, (long) objInfoPlayer.getGlobalDistribution().getTotal(4));
        assertEquals(13.9, objInfoPlayer.getGlobalDistribution().getPercent(4), 0.0);
        // globalTotal5
        assertEquals(145L, (long) objInfoPlayer.getGlobalDistribution().getTotal(5));
        assertEquals(11.5, objInfoPlayer.getGlobalDistribution().getPercent(5), 0.0);
        // globalTotal6
        assertEquals(107L, (long) objInfoPlayer.getGlobalDistribution().getTotal(6));
        assertEquals(8.5, objInfoPlayer.getGlobalDistribution().getPercent(6), 0.0);
        // globalTotal7
        assertEquals(87L, (long) objInfoPlayer.getGlobalDistribution().getTotal(7));
        assertEquals(6.9, objInfoPlayer.getGlobalDistribution().getPercent(7), 0.0);
        // globalTotal8
        assertEquals(77L, (long) objInfoPlayer.getGlobalDistribution().getTotal(8));
        assertEquals(6.1, objInfoPlayer.getGlobalDistribution().getPercent(8), 0.0);
        // globalTotal9
        assertEquals(55L, (long) objInfoPlayer.getGlobalDistribution().getTotal(9));
        assertEquals(4.4, objInfoPlayer.getGlobalDistribution().getPercent(9), 0.0);
        // globalTotal10
        assertEquals(59L, (long) objInfoPlayer.getGlobalDistribution().getTotal(10));
        assertEquals(4.7, objInfoPlayer.getGlobalDistribution().getPercent(10), 0.0);
        // globalTotal
        assertEquals(1264L, (long) objInfoPlayer.getGlobalDistribution().getTotal());
        // globalAvg
        assertEquals(5.65, objInfoPlayer.getGlobalDistribution().getAvg(), 0.0);

        // stats
        final List<PlayerStats> playerStats = objInfoPlayer.getAllStats();
        assertEquals(3L, (long) playerStats.size());

        PlayerStats stats = playerStats.get(0);
        // label
        assertEquals("2016-4", stats.getLabel());
        // rank
        assertEquals(90L, (long) stats.getPosition());
        // score
        assertEquals(140.42, stats.getScore(), 0.0);
        // played
        assertEquals(632L, (long) stats.getPlayed());
        // won
        assertEquals(104L, (long) stats.getWon());
        // wonPercent
        assertEquals(16.5, stats.getWonPercent(), 0.0);
        // placement
        assertEquals(3606L, (long) stats.getPlacement());
        // average
        assertEquals(5.71, stats.getAverage(), 0.0);

        stats = playerStats.get(1);
        // label
        assertEquals("2017-1", stats.getLabel());
        // rank
        assertEquals(144L, (long) stats.getPosition());
        // score
        assertEquals(132.24, stats.getScore(), 0.0);
        // played
        assertEquals(342L, (long) stats.getPlayed());
        // won
        assertEquals(53L, (long) stats.getWon());
        // wonPercent
        assertEquals(15.5, stats.getWonPercent(), 0.0);
        // placement
        assertEquals(1862L, (long) stats.getPlacement());
        // average
        assertEquals(5.44, stats.getAverage(), 0.0);

        stats = playerStats.get(2);
        assertEquals("Current", stats.getLabel());
        assertEquals(65L, (long) stats.getPosition());
        assertEquals(139.58, stats.getScore(), 0.0);
        assertEquals(290L, (long) stats.getPlayed());
        assertEquals(47L, (long) stats.getWon());
        assertEquals(16.2, stats.getWonPercent(), 0.0);
        assertEquals(1675L, (long) stats.getPlacement());
        assertEquals(5.78, stats.getAverage(), 0.0);

        info.add(DataExtractor.extractInfo(String.format("%s/test5", TestDataExtractor.BASE_URI)));
        DataExtractor.completeStats(info);

        assertEquals(2L, (long) info.size());

        final Iterator<PlayerInfo> iterator = info.iterator();
        objInfoPlayer = iterator.next();

        // login
        assertEquals("t/est\"1", objInfoPlayer.getLogin());
        // id
        assertEquals(47L, (long) objInfoPlayer.getId());
        // placement
        assertEquals(1675L, (long) objInfoPlayer.getCurrentStats().getPlacement());
        // targetPosition
        assertEquals(3L, (long) objInfoPlayer.getCurrentStats().getTargetPosition());
        // currentTrend
        assertEquals(4.2, objInfoPlayer.getCurrentTrend().getAvg(), 0.0);

        objInfoPlayer = iterator.next();

        assertEquals("schoumat", objInfoPlayer.getLogin());
        assertEquals(9751L, (long) objInfoPlayer.getId());
        assertEquals(8451L, (long) objInfoPlayer.getCurrentStats().getPosition());
    }

    @Path("")
    public static final class EndPoints {
        private Response get(final String path) throws URISyntaxException, IOException {
            final URL url = EndPoints.class.getResource(path);
            return Response.ok(Files.readAllBytes(Paths.get(url.toURI()))).build();
        }

        @GET
        @Produces(MediaType.TEXT_HTML)
        @Path("test1")
        public Response getTest1() throws Exception {
            return this.get("/test1.html");
        }

        @GET
        @Produces(MediaType.TEXT_HTML)
        @Path("test2")
        public Response getTest2() throws Exception {
            return this.get("/test2.html");
        }

        @GET
        @Produces(MediaType.TEXT_HTML)
        @Path("test3")
        public Response getTest3() throws Exception {
            return this.get("/test3.html");
        }

        @GET
        @Produces(MediaType.TEXT_HTML)
        @Path("test4")
        public Response getTest4() throws Exception {
            return this.get("/test4.html");
        }

        @GET
        @Produces(MediaType.TEXT_HTML)
        @Path("test5")
        public Response getTest5() throws Exception {
            return this.get("/test5.html");
        }
    }
}
