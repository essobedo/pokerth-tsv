/**
 * Copyright (C) 2017 essobedo.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.gitlab.essobedo.ptsv;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * @author Nicolas Filotto (nicolas.filotto@gmail.com)
 * @version $Id$
 * @since 1.0
 */
public final class TestRankingDistribution {

    @Test
    public void score() throws Exception {
        assertEquals(0.0, RankingDistribution.getScore(0, 0), 0.0);
        assertEquals(71.25, RankingDistribution.getScore(57, 10), 0.0);
        assertEquals(111.23, RankingDistribution.getScore(307, 59), 0.0);
        assertEquals(146.69, RankingDistribution.getScore(487, 73), 0.0);
        assertEquals(10.29, RankingDistribution.getScore(7, 7), 0.0);
    }

    @Test
    public void targetPosition() throws Exception {
        assertEquals(7L, (long) RankingDistribution.getTargetPosition(0.0, 0, 0));
        assertEquals(5L, (long) RankingDistribution.getTargetPosition(71.25, 57, 10));
        assertEquals(3L, (long) RankingDistribution.getTargetPosition(111.23, 307, 59));
        assertEquals(3L, (long) RankingDistribution.getTargetPosition(146.69, 487, 73));
        assertEquals(7L, (long) RankingDistribution.getTargetPosition(10.29, 7, 7));
    }
}
