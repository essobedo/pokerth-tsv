# PokerTH table's statistics viewer

## What is it for?

This project aims to generate a more details comparison page between all the opponents of the same table.

## What do I need?

The only thing to install is the application itself that you can install thanks to the dmg file for Mac OS, msi file for Windows and rpm file for Linux.

## How to launch it?

Simply double click on the executable file that has been installed

You will get an User Interface asking for the parameters or the URL of the normal comparison page of PokerTH something 
like *https://www.pokerth.net/component/pthranking/?view=pthranking&layout=gametable&nick1=val1&nick2=val2&nick3=val3&nick4=val4&nick5=val5&nick6=val6&nick7=val7&nick8=val8&nick9=val9&nick10=val10&table=val11*

Once the value entered simply click on *Extract Data*.

## How to use it?

The UI is composed of the stacks information, a table of statistics, a set of line charts and a set of pie charts.

### The stacks information

The stacks information is composed of the average stack and the total stack. When a player is deleted (see how in the next section), the
average stack is automatically updated.

In case you created a specific table with a different start stack or an player has left the game without losing all his chips,
you can still edit the total stack by clicking on the value. If the value that you provide starts with '-' or '+' it will respectively
remove or add the provided amount to the current value otherwise it will replace the actual value with the one provided. To validate your 
value, simply click outside the text field or press enter.

A refresh button has been added to allow us to refresh the data of the remaining players just in case previous games ended during the current game.

### The table of statistics
 
The table of statistics contains the following sortable (in both side ascending and descending) columns:
 
 * Position
 * Login
 * Score
 * Current Trend: Average score of the 5 last games
 * Season Avg: Average score of the current season
 * Global Avg: Average score of all seasons 
 * Season Games: Total amount of games played in the current season
 * Total Games: Total amount of games played in all seasons
 * Total Points: Total amount of points in the current season
 * Target Position: Position to at least reach to improve the current score 
 * Id
 
Those columns can be sorted by simply clicking on the column header, a second click on the same header will revert the sorting.
   
Finally the table contains a last special column called "Del" for "Delete" allowing to remove a player from the list which will automatically
update also the charts consequently.

### The line charts

There are 6 line charts allowing to compare the players over the seasons on several different criteria which are:

* Average
* Score
* Placement
* Won Percent
* Won
* Played

### Note:

The line charts are interactive.

### The pie charts

We have 2 pie charts for each player, the first one for the "Current Distribution" also known as "Statistics for current season"
and the second one for the "Global Distribution" also known as "Statistics for all time".

### Note:

The pie charts are interactive.

### Developed with ![](ij.png)
